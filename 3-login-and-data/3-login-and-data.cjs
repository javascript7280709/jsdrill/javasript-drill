/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/
const fs = require("fs");

function createAndDeleteFiles(numFiles = 2) {
  const fileNames = Array.from({ length: numFiles }, (value, index) => {
    return `file${index + 1}.txt`;
  });
  let filePromises = fileNames.map((file) => {
    return new Promise((resolve, reject) => {
      fs.writeFile(file, `${file}`, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  });
  Promise.all(filePromises)
    .then(() => {
      console.log("files created");
    })
    .catch((err) => {
      console.error(err);
    });

  setTimeout(() => {
    let deletionPromises = fileNames.map((file) => {
      return new Promise((resolve, reject) => {
        fs.unlink(file, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    });
    Promise.all(deletionPromises)
      .then(() => {
        console.log("Files deleted");
      })
      .catch((err) => {
        console.error(err);
      });
  }, 2 * 1000);
}
// createAndDeleteFiles();

/*

Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function readWriteAndDeleteFile(fileName = "lipsum.txt") {
  const lipsumPromise = new Promise((resolve, reject) => {
    let lipsumFileData =
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est perferendis repudiandae at consequatur ab molestiae. Obcaecati rerum maiores debitis possimus, inventore pariatur beatae ea quasi dolor iure excepturi non culpa ducimus cum vitae earum commodi perferendis officia necessitatibus nesciunt sunt fugiat suscipit cumque! Delectus, officiis? Dolore odio sunt illum dolores repudiandae veritatis culpa nobis et, tempora eum recusandae officia voluptates perferendis excepturi quos ducimus ab veniam obcaecati, nam rerum neque. Ipsa accusantium pariatur nam aut molestias officiis quasi neque exercitationem aliquam. Pariatur accusantium cum officiis voluptas, veritatis eaque nesciunt ex totam praesentium deserunt ratione architecto voluptatem maiores impedit voluptatum in optio. Dolores quaerat quos exercitationem? Quos magnam hic et suscipit tempore, officiis nihil maxime incidunt reprehenderit eligendi consequuntur ipsa, aspernatur itaque architecto a corporis! Autem qui ut reiciendis repudiandae hic possimus nihil fugit excepturi eaque, at, iure harum reprehenderit accusamus vitae ullam sit sunt distinctio similique earum eveniet. Id veniam quas facere tempore maiores eveniet fugiat odit corporis quos laudantium distinctio quisquam provident accusamus laborum doloremque aperiam, ex sunt? Odit, illo porro. Repellat rem itaque recusandae eius, alias assumenda perspiciatis sapiente ipsa minus vel placeat? Non repudiandae quam alias necessitatibus debitis cum atque pariatur laudantium quas hic fugiat illo sint maxime magni doloribus, modi vel iusto totam expedita ipsam exercitationem eius. Commodi saepe sed dolorum quibusdam, vel modi nam! Impedit architecto quasi est saepe repellat, atque ex vitae assumenda vero omnis hic nam illum eaque quis aperiam esse sapiente maxime modi. Dicta quo illo autem numquam hic. Aut quam culpa quae quos aspernatur. Ipsum architecto quibusdam, ipsam tenetur neque et eaque a? Quas corrupti eos accusamus nostrum sapiente nihil perspiciatis. Adipisci fugit expedita ex sed doloribus, maxime doloremque. Sapiente facere neque itaque, ducimus ex, molestiae architecto vero culpa esse officiis, dicta accusantium porro. Incidunt et iure enim corporis. Eos, iste at. Ut blanditiis nesciunt officia quasi unde fuga laudantium mollitia ducimus illum vero praesentium aut eos beatae omnis sint architecto iusto corporis ipsum molestiae at, dolorem necessitatibus excepturi amet facere. Magnam perferendis sequi praesentium maxime excepturi facilis expedita enim amet ipsum, laudantium perspiciatis vero assumenda repellat? Impedit sunt cupiditate ipsum consequuntur animi iste, sint modi illum ducimus. Consequuntur quaerat totam, ut vitae vel provident voluptates maxime amet voluptas, illum quae corporis minima blanditiis vero tempora minus porro non debitis. Ab totam aliquid reprehenderit adipisci! Culpa ex blanditiis ad accusamus odio provident tempora expedita magnam ipsa illo, autem porro dolorum? Nesciunt, asperiores! Nobis pariatur veritatis cum ex quia. Assumenda quibusdam sed consequuntur nam impedit rem expedita natus aliquam incidunt, dolorem nemo ipsam facere esse at necessitatibus maiores vitae nostrum provident similique adipisci quidem alias sunt? Non illo a, quas eum quo nostrum qui, suscipit corrupti libero cupiditate culpa! Non iusto, quasi iste, quod architecto a eligendi fugit aspernatur perferendis optio expedita ex in repudiandae nihil earum tempore possimus autem corrupti. A id modi vitae adipisci accusantium numquam dignissimos animi provident, ex assumenda quod dolor libero velit eum autem similique nemo facere asperiores ratione voluptate quaerat. Nulla ratione quas minus repellat nihil.";
    fs.writeFile(fileName, lipsumFileData, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
  lipsumPromise
    .then(() => {
      console.log("file created");
      return new Promise((resolve, reject) => {
        fs.readFile(fileName, "utf-8", (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
    })
    .then((data) => {
      console.log("lipsum file read successfully");
      return new Promise((resolve, reject) => {
        fs.writeFile("newLipsum.txt", data, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
    })
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.unlink(fileName, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    })
    .catch((err) => {
      console.error(err);
    });
}
// readWriteAndDeleteFile();

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/
function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

// A. login with value 3 and call getData once login is successful
function logData(user, activity) {
  // use promises and fs to save activity in some file
  return new Promise((resolve,reject)=>{
    user= {...user,...activity};
    fs.appendFile('UserLogData.log',JSON.stringify(user,null,4),(err)=>{
        if(err){
            reject(err);
        }
        else{
            resolve();
        }
    });
  });
}

const user = {
  id: 3,
  name: "shubham",
};

function userLogin(user) {

    login(user, 3)
    .then(() => {
      console.log("Successfully logged");
      let userData = {...user};
      let activity={
        login : 'Success',
        getData : 'Success',
        time : new Date()
    }
      logData(userData,activity)
      getData();
    })
    .catch((err) => {
        let userData = {...user};
        let activity = {
            login : 'failure',
            getData : 'failure',
            time : new Date()
        }
        logData(userData,activity)
    });
}
// userLogin(user);