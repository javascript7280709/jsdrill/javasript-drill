const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];




// 1. Get all items that are available 
const availableItems = items.filter((item) => {
    if (item.available) {
        return item;
    }
});

//2. Get all items containing only Vitamin C.
const itemsConatiningVitaminC = items.filter((item) => {
    if (item.contains === 'Vitamin C') {
        return item;
    }
});

// 3. Get all items containing Vitamin A.
const itemsConatiningVitaminA = items.filter((item) => {
    if (item.contains.includes('A')) {
        return item;
    }
});

// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }    
//     and so on for all items and all Vitamins.

let allVitamins = [];
items.map((item) => {
    let vitamins = item.contains;
    if (item.contains.includes(',')) {
        vitamins = item.contains.split(', ');
        allVitamins.push(...vitamins);
    }
    else {
        allVitamins.push(vitamins);
    }
});

allVitamins = [...new Set(allVitamins)];

const itemGroupsWithVitamins = allVitamins.map((vitamin) => {
    vitaminAndItems = {};
    vitaminContains = [];
    items.filter((item) => {
        if (item.contains.includes(vitamin)) {
            vitaminContains.push(item.name);
        }
    });
    vitaminAndItems[vitamin] = vitaminContains;
    return vitaminAndItems;
});
console.log(itemGroupsWithVitamins);

// 5. Sort items based on number of Vitamins they contain.
items.sort((firstItem, secondItem) => {
    return firstItem.contains.split(', ').length - secondItem.contains.split(', ').length;
});