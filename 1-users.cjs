const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


//Q1 Find all users who are interested in playing video games.
let userInterestedInPlayingVideoGames = Object.entries(users).filter((user) => {
    let interest = ''
    if (user[1].interest) {
        interest = user[1].interest[0];
    }
    else {
        interest = user[1].interests[0];
    }
    if (interest.includes('Playing Video Games')) {
        return user;
    }
});
userInterestedInPlayingVideoGames = Object.fromEntries(userInterestedInPlayingVideoGames);




//Q2 Find all users staying in Germany.
let usersStayingInGermany = Object.entries(users).filter((user) => {
    if (user[1].nationality === "Germany") {
        return user;
    }
});
usersStayingInGermany = Object.fromEntries(usersStayingInGermany);



// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
let rankingBasedOnDesignation = Object.entries(users).map((user) => {
    let designation = user[1].desgination;
    if (designation.includes('Senior') && designation.includes('Developer')) {
        user[1]['rank'] = 1;
    }
    else if (designation.includes('Developer')) {
        user[1]['rank'] = 2;
    }
    else {
        user[1]['rank'] = 3;
    }
    return user;
});


const sortedUsers = Object.entries(users)
    .map(([name, user]) => {
        return ({ name, ...user });
    })
    .sort((userA, userB) => {
        if (userA.rank !== userB.rank) {
            return userA.rank - userB.rank;
        }
        else {
            return userB.age - userA.age;
        }
    })
    .reduce((sortedUser, { name, ...user }) => {
        return ({ ...sortedUser, [name]: user });
    }, {});
console.log(sortedUsers);


// Q4 Find all users with masters Degree.
let usersWithMastersDegree = Object.entries(users).filter((user) => {
    if (user[1].qualification === "Masters") {
        return user;
    }
});
usersWithMastersDegree = Object.fromEntries(usersWithMastersDegree);


// Q5 Group users based on their Programming language mentioned in their designation.
const allUsers = Object.entries(users)
    .map(([name, user]) => {
        return ({ name, ...user });
    });

let languages = allUsers.map((user)=>{
    let language = user.desgination.split(' ');
    if(language[language.length-1] === 'Developer'){
        return language[language.length-2]
    }
    else{
        return language[language.length-1];
    }
});
languages = [...new Set(languages)];
const userByLanguages = languages.reduce((userByLanguages, language)=>{
    let userByLanguage = {};
    userByLanguage[language]=allUsers.filter((user) => {
        if (user.desgination.includes(language)) {
            return user;
        }
    });
    userByLanguages= Object.assign(userByLanguages, userByLanguage);
    return userByLanguages;
},{});

