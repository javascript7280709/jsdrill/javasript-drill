const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


// NOTE: For all questions, the returned data must contain all the movie information including its name.

const movies = Object.entries(favouritesMovies).map(([name,movie])=>{
    return ({name,...movie});
});
// Q1. Find all the movies with total earnings more than $500M. 
const moviesWithEarningMoreThan500M = movies.filter((movie)=>{
    movieEarning = Number((movie.totalEarnings
                                .split('$'))[1]
                                .replace('M',''));
    movie['earning']=movieEarning;
    return movieEarning > 500 ;
});
// console.log(moviesWithEarningMoreThan500M);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const moviesWithMoreThanThreeOscarNominations = moviesWithEarningMoreThan500M.filter((movie)=>{
    return movie.oscarNominations > 3;
});
// console.log(moviesWithMoreThanThreeOscarNominations);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
const moviesOfLeonardoDicaprio = movies.filter((movie)=>{
    return (movie.actors).includes("Leonardo Dicaprio");
});
// console.log(moviesOfLeonardoDicaprio);

// Q.4 Sort movies (based on IMDB rating)
//     if IMDB ratings are same, compare totalEarning as the secondary metric.
movies.sort((movieA,movieB)=>{
    if(movieA.imdbRating > movieB.imdbRating){
        return -1;
    }
    else if(movieA.imdbRating < movieB.imdbRating){
        return 1;
    }
    else{
        if(movieA.earning > movieB.earning){
            return -1;
        }
        else if(movieA.earning < movieB.earning){
            return 1;
        }
        else{
            return 0;
        }
    }
    
});
// console.log(movies);

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//     drama > sci-fi > adventure > thriller > crime


const moviesSortedByGenre = movies.reduce((moviesSortedByGenre,movie)=>{
    if(movie.genre.includes('drama')){
        if(moviesSortedByGenre['drama']){
            moviesSortedByGenre['drama'].push(movie);
        }
        else{
            moviesSortedByGenre['drama']=[];
            moviesSortedByGenre['drama'].push(movie);
        }
    }
    else if(movie.genre.includes('sci-fi')){
        if(moviesSortedByGenre['sci-fi']){
            moviesSortedByGenre['sci-fi'].push(movie);
        }
        else{
            moviesSortedByGenre['sci-fi']=[];
            moviesSortedByGenre['sci-fi'].push(movie);
        }
    }
    else if(movie.genre.includes('adventure')){
        if(moviesSortedByGenre['adventure']){
            moviesSortedByGenre['adventure'].push(movie);
        }
        else{
            moviesSortedByGenre['adventure']=[];
            moviesSortedByGenre['adventure'].push(movie);
        }
    }
    else if(movie.genre.includes('thriller')){
        if(moviesSortedByGenre['thriller']){
            moviesSortedByGenre['thriller'].push(movie);
        }
        else{
            moviesSortedByGenre['thriller']=[];
            moviesSortedByGenre['thriller'].push(movie);
        }
    }
    else if(movie.genre.includes('crime')){
        if(moviesSortedByGenre['crime']){
            moviesSortedByGenre['crime'].push(movie);
        }
        else{
            moviesSortedByGenre['crime']=[];
            moviesSortedByGenre['crime'].push(movie);
        }
    }
    return moviesSortedByGenre;
},{});
// console.log(moviesSortedByGenre);
//NOTE: Do not change the name of this file
