const fs = require('fs');
const givenData = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}
fs.writeFile('data.json',JSON.stringify(givenData),(err)=>{
    fs.readFile('data.json', 'utf-8', (err, data) => {
        if (err) {
            console.error(err.message);
        }
        else {
            data = JSON.parse(data);
            data = data.employees;
            let requiredData = data.filter((currentData) => {
                return [1, 13, 23].includes(currentData.id);
            });
            fs.writeFile('dataOfRequiredIds.json', JSON.stringify(requiredData), (err) => {
                if (err) {
                    console.error(err.message);
                } else {
                    dataGroupedByCompany = data.reduce((dataGroupedByCompany, currentEmployee) => {
                        if (!dataGroupedByCompany[currentEmployee.company]) {
                            dataGroupedByCompany[currentEmployee.company] = [];
                            dataGroupedByCompany[currentEmployee.company].push(currentEmployee);
                        }
                        else {
                            dataGroupedByCompany[currentEmployee.company].push(currentEmployee);
                        }
                        return dataGroupedByCompany;
                    }, {});
                    fs.writeFile('dataGroupedByCompany.json', JSON.stringify(dataGroupedByCompany), (err) => {
                        if (err) {
                            console.error(err.message);
                        }
                        else {
                            const powerPuffBrigade = data.filter((employee) => {
                                return employee.company === 'Powerpuff Brigade';
                            });
                            fs.writeFile('PowerPuffBrigade.json', JSON.stringify(powerPuffBrigade), (err) => {
                                if (err) {
                                    console.error(err);
                                }
                                else {
                                    const dataWithouIdTwo = data.filter((employee) => {
                                        return employee.id !== 2;
                                    })
                                    fs.writeFile('dataWithouIdTwo.json', JSON.stringify(dataWithouIdTwo), (err) => {
                                        if (err) {
                                            console.error(err);
                                        }
                                        else {
                                            const sortDataByCompanyName = data.sort((employeeA, employeeB) => {
                                                if (employeeA.company < employeeB.company) {
                                                    return -1;
                                                }
                                                if (employeeA.company > employeeB.company) {
                                                    return 1;
                                                }
                                                else {
                                                    if (employeeA.id < employeeB) {
                                                        return -1;
                                                    }
                                                    else {
                                                        return 1;
                                                    }
                                                }
                                            });
                                            fs.writeFile('SortedByCompanyAndID.json', JSON.stringify(sortDataByCompanyName), (err) => {
                                                if (err) {
                                                    console.error(err);
                                                }
                                                else {
                                                    const indexd92 = data.findIndex((employee) => {
                                                        return employee.id === 92;
                                                    });
                                                    const indexd93 = data.findIndex((employee) => {
                                                        return employee.id === 93;
                                                    });
                                                    let temp = data[indexd92];
                                                    data[indexd92] = data[indexd93];
                                                    data[indexd93] = temp;

                                                    fs.writeFile('SwappedData.json', JSON.stringify(data), (err) => {
                                                        if (err) {
                                                            console.error(err);
                                                        }
                                                        else {
                                                            const addDOBToEvenId = data.map((employee) => {
                                                                if (employee.id % 2 === 0) {
                                                                    employee['birthday'] = new Date().toLocaleDateString();
                                                                }
                                                                return employee;
                                                            })
                                                            fs.writeFile('EmployeeBirthdayWithEvenId.json', JSON.stringify(addDOBToEvenId), (err) => {
                                                                if (err) {
                                                                    console.error(err);
                                                                }
                                                                else {
                                                                    console.log('finished');
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});