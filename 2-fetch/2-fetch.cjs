/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended

*/
const fs = require('fs');
const path = require('path');

function fetchData(url) {
  return fetch(url)
    .then((response) => {
      let data = response.json();
      return data;
    })
    .catch((err) => {
      console.error(err);
    });
}

function writeInFile(fileName, data) {
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, (err) => {
      if (err) {
        reject(new Error (err));
      } else {
        resolve();
      }
    });
  });
}
// 1. Fetch all the users
function fetchUser(fileName, url) {
  fetchData(url)
    .then((data) => {
      data = JSON.stringify(data);
      return writeInFile(fileName, data);
    })
    .catch((err) => {
      console.error(err);
    });
}
// fetchUser('usersJson.json','https://jsonplaceholder.typicode.com/users');

// 2. Fetch all the todos
function fetchTodo(fileName) {
  fetchData(url)
    .then((data) => {
      return writeInFile(fileName, data);
    })
    .catch((err) => {
      console.error(err);
    });
}
// fetchTodo('todoJson.json','https://jsonplaceholder.typicode.com/todos');

function fetchUserAndThenTodos(userUrl,userFile, todoUrl,todoFile) {
  fetchData(userUrl)
    .then((data) => {
      return writeInFile(userFile, data);
    })
    .then(() => {
      return fetchData(todoUrl);
    })
    .then((data) => {
      return writeInFile(todoFile, data);
    })
    .catch((err)=>{
      console.error(err);
    });
}
// fetchUserAndThenTodos('https://jsonplaceholder.typicode.com/users','userJson.json','https://jsonplaceholder.typicode.com/todos','todoJson.json');

// 4. Use the promise chain and fetch the users first and then all the details for each user.
function fetchUsersAndTheirDetails(userUrl){
  fetchData(userUrl)
  .then((data)=>{
    let userPromises = data.map((user)=>{
      return fetchData(`https://jsonplaceholder.typicode.com/users?id=${user.id}`)
    });
    return Promise.all(userPromises);
  })
  .then((data)=>{
    return writeInFile('UsersAndTheirDetails.json',data);
  })
  .catch((err)=>{
    console.error(err);
  });
}
// fetchUsersAndTheirDetails('https://jsonplaceholder.typicode.com/users');

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo
function userDetailsOfNumberedTodo(todoUrl,number){
  if(Number.isInteger(number)){
    number = ''+number;
  }
  fetchData(path.join(todoUrl,number))
  .then((data)=>{
    return fetchData(`https://jsonplaceholder.typicode.com/users?id=${data.userId}`);
  })
  .then((data)=>{
    writeInFile('userDetailsOfNumberedTodo.json',data);
  })
  .catch((err)=>{
    console.error(err);
  });
}
// userDetailsOfNumberedTodo('https://jsonplaceholder.typicode.com/todos','1');