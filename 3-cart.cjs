const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]



// Q1. Find all the items with price more than $65.
const itemsWithPriceMoreThan65 = products.reduce((itemsWithPriceMoreThan65, product) => {
    const items = (Object.keys(product));
    items.map((item) => {
        let price = 0;
        if (product[item].price) {
            let price = (product[item].price);
            price = price.split('$')[1];
            product[item].price = price;
            if (price > 65) {
                itemsWithPriceMoreThan65.push({ name: item, ...product[item] });
            }
        }
        else {
            let utensils = (product[item]);
            const currentProduct = utensils.filter((utensil) => {
                let utensilNames = Object.keys(utensil)[0];
                price = utensil[utensilNames].price.split('$')[1];
                utensil[utensilNames].price = price;
                if (price > 65) {
                    return ({ name: utensilNames, ...utensil[utensilNames] });
                }
            });
            itemsWithPriceMoreThan65.push(currentProduct);
        }
    });
    return itemsWithPriceMoreThan65;
}, [])
.flat();
// console.log(itemsWithPriceMoreThan65);


// Q2. Find all the items where quantity ordered is more than 1.
const itemsOrderMoreThanOne = products.reduce((itemsOrderMoreThanOne, product) => {
    const items = (Object.keys(product));
    items.map((item) => {
        let quantityOrdered = 0;
        if (product[item].quantity) {
            let quantityOrdered = (product[item].quantity);
            if (quantityOrdered > 1) {
                itemsOrderMoreThanOne.push({ name: item, ...product[item] });
            }
        }
        else {
            let utensils = (product[item]);
            const currentProduct = utensils.filter((utensil) => {
                let utensilNames = Object.keys(utensil)[0];
                quantityOrdered = utensil[utensilNames].quantity;
                if (quantityOrdered > 1) {
                    return ({ name: utensilNames, ...utensil[utensilNames] });
                }
            });
            itemsOrderMoreThanOne.push(currentProduct);
        }
    });
    return itemsOrderMoreThanOne;
}, [])
.flat();
// console.log(itemsOrderMoreThanOne);


// Q.3 Get all items which are mentioned as fragile.
const fragileItems = products.reduce((fragileItems, product) => {
    const items = (Object.keys(product));
    items.map((item) => {
        if (product[item].type) {
            if (product[item].type === 'fragile') {
                fragileItems.push({ name: item, ...product[item] });
            }
        }
        else if (Array.isArray((product[item]))) {
            let utensils = (product[item]);
            let currentProduct = utensils.filter((utensil) => {
                let utensilNames = Object.keys(utensil)[0];
                if (utensil[utensilNames].type === 'fragile') {
                    return ({ name: utensilNames, ...utensil[utensilNames] });
                }
            });
            fragileItems.push(currentProduct);
        }
    });
    return fragileItems;
}, [])
.flat();
// console.log(fragileItems);

// Q.4 Find the least and the most expensive item for a single quantity.
const leastAndMostExpensive = {};
const allItemsByPrice = products.reduce((itemsWithPriceMoreThan65, product) => {
    const items = (Object.keys(product));
    items.map((item) => {
        if (!(Array.isArray(product[item]))) {
            let priceOfSingleUnit = product[item].price/product[item].quantity;
            itemsWithPriceMoreThan65.push({ name: item, ...product[item],'priceOfSingleUnit' : priceOfSingleUnit });
        }
        else {
            let utensils = (product[item]);
            let currentProduct = utensils.map((utensil) => {
                let utensilNames = Object.keys(utensil)[0];
                let priceOfSingleUnit = utensil[utensilNames].price/ utensil[utensilNames].quantity;
                return ({ name: utensilNames, ...utensil[utensilNames],'priceOfSingleUnit' : priceOfSingleUnit});
            });
            product[item.name] = currentProduct;
            itemsWithPriceMoreThan65.push(...currentProduct);
        }
    });
    return itemsWithPriceMoreThan65;
}, []).sort((itemA,itemB) =>{
    if(itemA.priceOfSingleUnit > itemB.priceOfSingleUnit){
        return -1;
    }
    else if(itemA.priceOfSingleUnit < itemB.priceOfSingleUnit){
        return 1;
    }
    else{
        return 0;
    }
});
leastAndMostExpensive['Least'] = allItemsByPrice[0];
leastAndMostExpensive['Most'] = allItemsByPrice[allItemsByPrice.length-1];
console.log(leastAndMostExpensive);


// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

// NOTE: Do not change the name of this file

